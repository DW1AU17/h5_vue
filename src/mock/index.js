
import Mock from 'mockjs'
import url from './url'

url.forEach(item => {
  let { url, type, response } = item
  Mock.mock(url, type, response)
})