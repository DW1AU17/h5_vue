import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@nutui/nutui/dist/nutui.css'
// import axios from './utils/request'  
import axios from 'axios'
import './mock'

import { Dialog, Picker, Cell, Avatar } from '@nutui/nutui'

Dialog.install(Vue)
Picker.install(Vue)
Cell.install(Vue)
Avatar.install(Vue)

Vue.config.productionTip = false
Vue.prototype.axios = axios

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
