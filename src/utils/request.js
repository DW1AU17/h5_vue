import axios from 'axios'

// 创建axios实例
const request = axios.create({
  base: process.env.VUE_APP_BASE_URL,
  timeout: 50000     // 请求超市
})

// 请求拦截
request.interceptors.request.use(
  config => {
    if (getToken()) {
      config.headers.Authorization = getToken()
    }
    return config
  },
  error => Promise.reject(error)
)

// 相应拦截
request.interceptors.response.use(res => {
  // 判断token是否过期
  if (res.data.status === 403) {
    removeToken()
  }
  return res
})

export default request